class Esercitazione1 {


    String parserXml() {

        //String fileContents = new File('./inputs/ekb.xml').getText('UTF-8');
        
        //ciao

        def rootNode = new XmlSlurper().parse('./inputs/ekb.xml');
        def ArrayList<ArrayList<String>> matrice = new ArrayList<ArrayList<String>>();


        def quasiRoot = rootNode.'**'.findAll{ elemento -> elemento.tipo.text() == "TEST_OI_TABELLA"};

        quasiRoot.each{

            def tabelle = it.'**'.findAll { it.name() == 'tabella' && it.@id == "TABLE1" };

            tabelle.each
                    {
                        def riga = new ArrayList<String>();

                        it.riga.each {
                            row ->
                                return row.cella.each { cell ->
                                    //println(cell);
                                    def collect = cell.'**'
                                            .findAll { it.name() == 'UNI' }
                                            .each { uni ->
                                        println("test:   " + uni.text())
                                        return uni.text()
                                    };

                                }

                        }
                        matrice.add(riga);
                    }
        }

        return fileContents;
    }
}
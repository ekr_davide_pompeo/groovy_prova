import java.nio.file.Path
import java.nio.file.Paths

/**
 * Created by Ekr z50 3 on 01/12/2015.
 */
//import com.opencsv.*

class Variante {

    String parserXml() {

        //leggo il file

        Path currentDir = Paths.get("").toAbsolutePath()
        Path csvDir = Paths.get(currentDir.toString(), "csv")

        Path inputDir = Paths.get(currentDir.toString(), 'src', 'inputs', 'ekb.xml')

        deleteDir(csvDir.toFile());
        csvDir.toFile().mkdirs()

        def ekb_piatto = new XmlSlurper().parse(inputDir.toString())

        // make a List of String[]'s
        //List bookings = (0..100).collect { return (String[]) ['things', 'stuff', 'here is a comma, """', it.toString()]}

        assert ekb_piatto instanceof groovy.util.slurpersupport.GPathResult

        def tabelle = ekb_piatto.'**'.findAll { node -> node.name() == 'tabella'  }

        tabelle.eachWithIndex{ def tabella, int i  ->
            //creo il file

            def ciao = tabella.@id

            Path p = Paths.get(csvDir.toString(), ciao.toString() + "_" + i + '.csv')

            p.toFile().withWriter { //path.tofile crea il file automaticamente nella path
                file ->
                    def celle = tabella.'*'
                            .findAll { node -> node.name() == 'riga' }
                            .each { riga ->
                        riga.'*'.findAll { node -> node.name() == 'cella' }
                                .each { cella ->
                            file.write( ((String)cella.text()).normalize().trim().replaceAll("( )+", " ").replace('\n','&'))
                            file.write( '; ')
                        }
                        file.write(System.getProperty('line.separator'))
                    }
            }
        }


    }

    void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }


//    void creaTabella() {
//
//    }


}
